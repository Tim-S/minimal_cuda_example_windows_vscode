cmake_minimum_required(VERSION 3.18 FATAL_ERROR)
project(cmake_and_cuda LANGUAGES CXX CUDA)
 
include(CTest)

find_package(CUDA REQUIRED)
include_directories("${CUDA_INCLUDE_DIRS}")

set(TARGET example)

# add_library(libexample STATIC
#   randomize.cpp
#   randomize.h
#   example.cu
#   example.h
#   v3.cu
#   )
 
# Request that libexample be built with -std=c++11
# As this is a public compile feature anything that links to 
# libexample will also build with -std=c++11
# target_compile_features(libexample PUBLIC cxx_std_11)
 
# We need to explicitly state that we need all CUDA files in the 
# example library to be built with -dc as the member functions 
# could be called by other libraries and executables
# set_target_properties( libexample
#                        PROPERTIES CUDA_SEPARABLE_COMPILATION ON)


add_executable(${TARGET} example.cu)
 
set_property(TARGET ${TARGET} PROPERTY CUDA_SEPARABLE_COMPILATION ON)
set_property(TARGET ${TARGET} PROPERTY CUDA_ARCHITECTURES OFF)

# target_link_libraries(${TARGET} PRIVATE libexample)
 
# if(APPLE)
#   # We need to add the path to the driver (libcuda.dylib) as an rpath, 
#   # so that the static cuda runtime can find it at runtime.
#   set_property(TARGET ${TARGET} 
#                PROPERTY
#                BUILD_RPATH ${CMAKE_CUDA_IMPLICIT_LINK_DIRECTORIES})
# endif()