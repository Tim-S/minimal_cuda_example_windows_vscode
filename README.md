# Minimal CUDA Example with vscode on Windows

## Prerequisites

* CMake

* Visual Studio 2019 (x64)

* Cuda Toolkit 11.5

## Build on Windows

Open "x64 Native Tools Command Prompt for VS 20019", type

```powershell
cd build
cmake .. -G Ninja
cmake --build .
```
